/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.bel_riose.geometry;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Ivan Gudoshnikov
 */
public class Vector implements Serializable, Cloneable {

    public double x, y;

    public static Vector sum(Vector a, Vector b) {
        return new Vector(a.x + b.x, a.y + b.y);
    }

    public static Vector sum(Vector a, Vector b, Vector c, Vector d) {
        return new Vector(a.x + b.x + c.x + d.x, a.y + b.y + c.y + d.y);
    }

    public static Vector mult(double k, Vector a) {
        return new Vector(k * a.x, k * a.y);
    }

    public static Vector rotate(Vector v, double angle) {
        return new Vector(Math.cos(angle) * v.x - Math.sin(angle) * v.y, Math.sin(angle) * v.x + Math.cos(angle) * v.y);
    }
    
    public static Vector diff(Vector a, Vector b){
        return new Vector(a.x-b.x,a.y-b.y);
    }
    
    public static double dotProd(Vector a, Vector b){
        return a.x*b.x+a.y*b.y;
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Vector v) {
        this.x = v.x;
        this.y = v.y;
    }

    public Vector addToMe(Vector a) {
        this.x += a.x;
        this.y += a.y;
        return this;
    }

    public Vector multMe(double k) {
        this.x *= k;
        this.y *= k;
        return this;
    }
    
    public Vector substractFromMe(Vector v){
        x-=v.x;
        y-=v.y;
        return this;
    }

    public Vector rotateMe(double angle) {
        double newX = Math.cos(angle) * x - Math.sin(angle) * y,
                newY = Math.sin(angle) * x + Math.cos(angle) * y;
        x = newX;
        y = newY;
        return this;
    }

    @Override
    public Object clone() {
        Object result = null;
        try {
            result = super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static class JSON_KEYS {

        public static final String x = "x", y = "y";
    }

    public static JSONObject toJson(Vector v) {
        JSONObject result = new JSONObject();
        result.put(JSON_KEYS.x, v.x);
        result.put(JSON_KEYS.y, v.y);
        return result;
    }

    public static Vector fromJson(JSONObject json) {
        return new Vector((Double) json.get(JSON_KEYS.x), (Double) json.get(JSON_KEYS.y));
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Vector) && ((Vector) obj).x == x && ((Vector) obj).y == y;
    }
}
