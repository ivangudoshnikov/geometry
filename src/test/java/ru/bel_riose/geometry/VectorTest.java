package ru.bel_riose.geometry;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.json.simple.JSONObject;

/**
 * Unit test for simple App.
 */
public class VectorTest
        extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public VectorTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(VectorTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testJson() {
        Vector v1 = new Vector(1.5, -100.7);
        JSONObject json = Vector.toJson(v1);
        Vector v2 = Vector.fromJson(json);
        v2.multMe(2);
        System.out.println(v2.x);
        System.out.println(v2.y);
        assertTrue((v2.x == 3) && (v2.y == -201.4));
    }

    public void testEquals() {
        Vector v11 = new Vector(1, 1),
                v12 = new Vector(1, 2),
                v21 = new Vector(2, 1),
                v22 = new Vector(2, 2),
                v11b = new Vector(1, 1);
        assertFalse(v11.equals(v12));
        assertFalse(v11.equals(v21));
        assertFalse(v11.equals(v22));
        assertTrue(v11.equals(v11b));
    }
}
